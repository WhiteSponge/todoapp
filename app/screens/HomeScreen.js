import React, { useState } from 'react';
import { Image, View, Text, StyleSheet, FlatList, TextInput, TouchableOpacity} from 'react-native';
import TodoItem from '../components/TodoItem';
import uuid from 'react-native-uuid';

const HomeScreen = () => {

    const [items, setItems] = useState([
        { id:uuid.v4(), text:'Buy milk'},
        { id:uuid.v4(), text:'Buy bread'},
        { id:uuid.v4(), text:'Finish homework'},
        { id:uuid.v4(), text:'Iron clothes'},
        { id:uuid.v4(), text:'Workout once per week'},
    ]);

    const [enteredText,setEnteredText] = useState("");

    const removeItem = (id) => {
    
        setItems(oldItems => {
            return oldItems.filter(item => item.id != id);
        })
    }

    const addItem = (text) => {
    
        setItems(oldItems => {
            return [{ id: uuid.v4(),text}, ...oldItems];
        })
    }

  return (
    <View style={styles.parentContainer}>
      
      <View style={styles.header}>
          <Image
            style={styles.image}
            source={{uri:"https://media.istockphoto.com/photos/random-multicolored-spheres-computer-generated-abstract-form-of-large-picture-id1295274245?b=1&k=20&m=1295274245&s=170667a&w=0&h=4t-XT7aI_o42rGO207GPGAt9fayT6D-2kw9INeMYOgo="}}
            />
          <Text style={styles.headerText}>Todo Items</Text>
      </View>
      <View style={styles.bodyContainer}>
      
          <FlatList
            data={items}
            renderItem={({item}) => <TodoItem item={item} removeItem={removeItem} />}
          />
      </View>
      <View style={styles.footerContainer}>
      
          <TextInput 
            value={enteredText}
            placeholder={"Add item"}
            style={styles.textInput}
            onChangeText={(text)=>setEnteredText(text)}
          />
          <TouchableOpacity 
            onPress={()=>{addItem(enteredText)}}
            style={styles.addButton}>
              <Text style={styles.addButtonText}>Add Item</Text>
          </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({

    image: {
        width:60,
        height:60,
        borderRadius:30
    },
    parentContainer: {
        flex:1,
        flexDirection:'column'
    },
    header: {
        width:'100%',
        height:'13%',
        paddingLeft:30,
        flexDirection:'row',
        marginTop:'20%'
    },
    headerText: {
        // color:'white',
        fontWeight:'600',
        fontSize:30,
        paddingLeft:15,
        paddingTop:10
    },
    bodyContainer: {
        paddingTop:0,
        width:'100%',
        height:'60%'
    },
    footerContainer: {
        paddingLeft:33,
        width:'100%',
        height:'20%',
    },
    textInput: {
        borderColor:'rgba(255,0,0,0.2)',
        borderWidth:1,
        padding:15,
        width:'90%',
        marginBottom:10,
    },
    addButton: {
        backgroundColor:'orange',
        padding:15,
        width:'90%',
        alignItems:'center'
    },
    addButtonText: {
        color:'white',
        fontWeight:'600'
    }
});

export default HomeScreen;